from enum import Enum, IntEnum, auto
import os
import os.path
import seaborn

class Accidental(Enum):
    NONE = auto()
    SHARP = auto()
    DOUBLE_SHARP = auto()
    FLAT = auto()
    DOUBLE_FLAT = auto()
ACC2STR = {Accidental.NONE:"", 
           Accidental.SHARP:"#", Accidental.DOUBLE_SHARP:"##",
           Accidental.FLAT:"b", Accidental.DOUBLE_FLAT:"bb"}
ACC2MUS = {Accidental.NONE:"", 
           Accidental.SHARP:"\\sh{*}", 
           Accidental.DOUBLE_SHARP:"\\dsh{*}",
           Accidental.FLAT:"\\fl{*}", 
           Accidental.DOUBLE_FLAT:"\\dfl{*}"}
ACC2OFF = {Accidental.NONE:0, 
           Accidental.SHARP:+1, Accidental.DOUBLE_SHARP:+2,
           Accidental.FLAT:-1, Accidental.DOUBLE_FLAT:-2}
OFF2ACC = {v: k for k, v in ACC2OFF.items()}

class NoteType(Enum):
    BASE = auto()
    UPPER = auto()
    AVOID = auto()
STR2TYPE = {"b":NoteType.BASE, "u":NoteType.UPPER, "a":NoteType.AVOID}
TYPE2STR = {v: k for k, v in STR2TYPE.items()}

NOTE2STR = ["C", "D", "E", "F", "G", "A", "B"]
NOTE2PITCH = {-2:'a', -1:'b', 0:'c', 1:'d', 2:'e', 3:'f', 4:'g', 5:'h', 6:'i', 7:'j', 8:'k', 9:'l', 10:'m', 11:'n', 12:'o'}
NOTE2ROMAN = ["\\barroman{%s}"%s for s in "I,II,III,IV,V,VI,VII,VIII,IX,X".split(",")]
NOTE_STEPS = [0, 2, 4, 5, 7, 9, 11]

TITLE_SCALE = "2"
MUSIC_SCALE = "0.7"

COLOR_INDICES = [0,0,1,0,2,0,3,0,4,0,5,0,0]
NUM_COLORS = 13
COLORS = [[1,1,1]] + seaborn.husl_palette(5, s=0.6,l=0.8)
print("Colors:", COLORS)
COLOR_NAMES = ["husl%d"%i for i in range(len(COLOR_INDICES))]
COLOR_DEFINITIONS = ["\definecolor{%s}{rgb}{%f,%f,%f}"%(n,COLORS[i][0],COLORS[i][1],COLORS[i][2]) for n,i in zip(COLOR_NAMES, COLOR_INDICES)]

class Note:
    octave = 0
    note = 0 #C
    accidental = Accidental.NONE
    type = NoteType.BASE

    def __str__(self):
        return "(%d:%d%s:%s)"%(self.octave, self.note, 
                               ACC2STR[self.accidental], 
                               TYPE2STR[self.type])

    def latex(self):
        note = self.note
        octave = self.octave
        acc = self.accidental
        #halfsteps, sf = transpose

        p = NOTE2PITCH[note%7 + 7*octave]
        outstr = ""
        if self.type==NoteType.AVOID:
            outstr += "\\lpar{%s}\\rpar{%s}"%(p,p)
        outstr += ACC2MUS[acc].replace('*',p)
        if self.type==NoteType.BASE:
            outstr += "\\nh{%s}"%p
        else:
            outstr += "\\nq{%s}"%p
        return outstr

class NoteRel:
    type = NoteType.BASE
    step = 0

    def realize(self, rootNote : Note, index : int):
        rootLetter = rootNote.note
        rootOff = ACC2OFF[rootNote.accidental]
        targetLetter = (rootLetter + index) % 7
        targetOff = rootOff + self.step
        if rootLetter <= targetLetter:
            octave = rootNote.octave
            acc = targetOff - (NOTE_STEPS[targetLetter] - NOTE_STEPS[rootLetter])
        else:
            octave = rootNote.octave + 1
            acc = targetOff - (12 - (NOTE_STEPS[rootLetter] - NOTE_STEPS[targetLetter]))
        note = Note()
        note.accidental = OFF2ACC[acc if acc < 3 else acc-12]
        note.note = targetLetter
        note.octave = octave-1 if rootLetter>=5 else octave
        note.type = self.type
        return note

class Chord:
    note = 0 #C
    accidental = Accidental.NONE
    extension = ""

    def __init__(self, note = 0, accidental = Accidental.NONE, extension = ""):
        self.note = note
        self.accidental = accidental
        self.extension = extension

    def __str__(self):
        return "[%d%s%s]"%(self.note, ACC2STR[self.accidental], self.extension)

    def latex(self, mode):
        if isinstance(self.note, str):
            return "\\writechord{%s%s%s}"%(self.note, ACC2STR[self.accidental], self.extension)
        elif mode=="roman":
            return "\\writechord{%s%s%s}"%(NOTE2ROMAN[self.note], ACC2STR[self.accidental], self.extension)
        elif mode=="bullet":
            return "\\writechord{{\\bullet}%s}"%self.extension
        else:
            return "\\writechord{%s%s%s}"%(NOTE2STR[self.note], ACC2STR[self.accidental], self.extension)

    def copy(self, newNote=None):
        c = Chord()
        c.note = newNote if newNote is not None else self.note
        c.accidental = self.accidental
        c.extension = self.extension
        return c

class Scale:
    def __init__(self, name : str, notes : str, chords : str, function : int):
        self.name = name
        self.function = function
        
        self.notes = []
        for s in notes.split(","):
            n = NoteRel()
            n.type = STR2TYPE[s[0]]
            off = 0
            if s[-2:]=='bb':
                off = -2
                s = s[0:-2]
            elif s[-1]=='b':
                off = -1
                s = s[0:-1]
            elif s[-1]=='#':
                off = +1
                s = s[0:-1]
            note = int(s[1:]) - 1
            n.step = NOTE_STEPS[note % 7] + off
            self.notes.append(n)

        self.chords = [Chord(extension=e) for e in chords.split(";")]

    def __str__(self):
        root = Note()
        return "Scale: %s\n  Notes=%s\n  Chords=%s"%(
            self.name, 
            ",".join([str(n.realize(root, i)) for i,n in enumerate(self.notes)]),
            ",".join([str(c) for c in self.chords]))

    def latex(self, base : str, color_index : int, marker_name):
        outstr = ""

        outstr += "\\cellcolor{%s}"%(COLOR_NAMES[(color_index - self.function)%12])

        rootNote = Note()
        if base[-2:]=='bb':
            rootNote.accidental = Accidental.DOUBLE_FLAT
            base = base[0:-2]
        if base[-1]=='b':
            rootNote.accidental = Accidental.FLAT
            base = base[0:-1]
        elif base[-1]=='#':
            rootNote.accidental = Accidental.SHARP
            base = base[0:-1]
        rootNote.note = NOTE2STR.index(base)
        outstr += "\\Centering\\scalebox{%s}{\\begin{music}\\parindent1mm\\instrumentnumber{1}\\nostartrule\\startextract\\Notes%%\n" % MUSIC_SCALE
        for i,n in enumerate(self.notes):
            outstr += n.realize(rootNote, i).latex()
        topNote = self.notes[0].realize(rootNote, 0)
        topNote.octave += 1
        outstr += topNote.latex()
        outstr += "%\n\\en\\zendextract\\end{music}}%\n"

        outstr += "\\tikzmark[xshift=\\tabcolsep, yshift=2pt]{%s}%%\n"%marker_name

        return outstr

mode = "hm"
if mode=="major":
    # Major / natural minor
    title = "Scales in Dur / nat\"urlich Moll"
    ION = Scale("ionisch", "b1,u9,b3,a11,b5,u13,b7", "maj7;6;(add9);(6/9);maj9;maj7(add13);^{maj13}", 0)
    DOR = Scale("dorisch", "b1,u9,b3b,u11,b5,a13,b7b", "mi7;mi(add9);mi7(add11);mi9;mi11", 2)
    PHRYG=Scale("phrygisch", "b1,a9b,b3b,u11,b5,a13b,b7b", "mi7;mi7(add11)", 4)
    LYD = Scale("lydisch", "b1,u9,b3,u11#,b5,u13,b7", "maj7;6;(add9);(6/9);maj9;ma^{6/9}(#11);maj9(#11);^{maj13}(#11)", -1)
    MIXO= Scale("mixolydisch", "b1,u9,b3,a11,b5,u13,b7b", "ma7;ma9;ma13", 1)
    AEOL= Scale("aeolisch (nat. moll)", "b1,u9,b3b,u11,b5,a13b,b7b", "mi7;mi(add9);mi9;mi7(add11);mi11", 3)
    LOKR= Scale("lokrisch", "b1,a9b,b3b,u11,b5b,a13b,b7b", "mi7(b5);mi7(b5,add11);mi7(b5,add11,addb13)", 5)
    COLUMNS = [("T", 0, ION), ("S", 1, DOR), ("T/D", 2, PHRYG), ("S", 3, LYD), ("D", 4, MIXO), ("T/S", 5, AEOL), ("D", 6, LOKR)]
elif mode=="mm":
    # melodic minor
    title = "Scales in Melodisch Moll"
    MM1 = Scale("melodisch moll", "b1,u2,b3b,a4,b5,u6,b7", "mimaj7;mi6;mi(add9);mimaj9;mi(6/9);mimaj9(add13)", 0)
    MM2 = Scale("dorisch(\\flat 2) / phygisch(\\natural 6)", "b1,a2b,b3b,u4,b5,u6,b7b", "mi7;mi9(sus4)", 2)
    MM3 = Scale("lydisch(\\sharp 5)", "b1,u2,b3,u4#,b5#,a6,b7", "ma(\\sharp5);maj7(#5);maj9(#5);maj9(#5,#11)", -3)
    MM4 = Scale("mixo(\\sharp 11) / lydisch(\\flat 7)", "b1,u2,b3,u4#,b5,u6,b7b", "ma7;ma9;ma7(add#11);ma9(add#11);ma13(#11)", -1)
    MM5 = Scale("mixo(\\flat 13)", "b1,u2,b3,u4,b5,u6b,b7b", "7;7(b13);9(b13)", 1)
    MM6 = Scale("lokrisch(\\natural 9)", "b1,u2,b3b,u4,b5b,u6b,b7b", "mi7(b5);mi9(b5);mi7(b5,add11);mi11(b5)", 3)
    MM7 = Scale("alteriert", "b1,u2b,b3b,u4b,b5b,a6b,b7b", "7;7(b5);7(#5);C7(#9,b9,#5,b5)", 5)
    COLUMNS = [("T", 0, MM1), ("S", 1, MM2), ("T/D", "{\\barroman{III}\\flat}", MM3), ("S", 3, MM4), ("D", 4, MM5), ("T/S", 5, MM6), ("D", 6, MM7)]
elif mode=="hm":
    # harmonic minor
    title = "Scales in Harmonisch Moll"
    HM1 = Scale("harmonisch moll", "b1,u2,b3b,a4,b5,a6b,b7", "mimaj7;mimaj9;mi(b6);mimaj7(b13)", 0)
    HM2 = Scale("lokrisch(\\natural 6)", "b1,u2b,b3b,u4,b5b,u6,b7b", "mi7(b5);mi7(b5,b9)", 2)
    HM3 = Scale("ionisch(\\sharp 5)", "b1,u2,b3,u4,b5#,u6,b7", "maj7(\\sharp5)", -3)
    HM4 = Scale("dorisch(\\sharp 11)", "b1,u2,b3b,u4#,b5,u6,b7b", "mi7;mi9;mi7(#11);mi9(#11),mi13(#11)", -1)
    HM5 = Scale("HM5 / mixo(\\flat 9/\\flat 13)", "b1,u2b,b3,a4,b5,u6b,b7b", "7;7(b9);7(b9,b13)", 1)
    HM6 = Scale("lydisch(\\sharp 9)", "b1,u2#,b3,u4#,b5b,u6,b7", "maj7;maj7(#9,#11)", -4)
    HM7 = Scale("HM7 / superlokrisch", "b1,a2b,b3b,a4b,b5b,u6b,b7bb", "o;o7;o7(addb13)", 5)
    COLUMNS = [("T", 0, HM1), ("S", 1, HM2), ("T/D", "{\\barroman{III}\\flat}", HM3), ("S", 3, HM4), ("D", 4, HM5), ("T/S", "{\\barroman{VI}\\flat}", HM6), ("D", 6, HM7)]

def saveLaTeX(contents : str):
    os.makedirs("out", exist_ok = True)
    with open("Template.tex") as f:
        template = f.read()
    with open("out/Main.tex", "w") as f:
        f.write(template.replace("<<<Title>>>", title).replace("<<<Body>>>", contents))

# Transposition over rows
ROWS = ["Gb", "Db", "Ab", "Eb", "Bb", "F", "C", "G", "D", "A", "E", "B", "F#"]
if mode=="major":
    ROWS_EXT = ["G\\flat/e\\flat", 
            "D\\flat/b\\flat", 
            "A\\flat/f", 
            "E\\flat/c", 
            "B\\flat/g", 
            "F/d", 
            "C/a", 
            "G/e", 
            "D/b", 
            "A/f\\sharp", 
            "E/c\\sharp", 
            "B/g\\sharp", 
            "F\\sharp/d\\sharp"]
else:
    ROWS_EXT = [r.replace("b","\\flat").replace("#","\\sharp") for r in ROWS]
ROW_BOLD_SEP = set([0]) # after which row a bold line will be inserted
# Scales over columns
COLUMN_BOLD_SEP = set([0])

# fill table
table = [["" for x in range(len(COLUMNS)+1)] for y in range(len(ROWS)+1)]
# write column header
for i,col in enumerate(COLUMNS):
    outstr = ""
    if mode=="major":
        outstr += "\\scalebox{%s}{"%TITLE_SCALE + col[0] + ", " + col[2].chords[0].copy(col[1]).latex("roman") + "}\\newline ";
        outstr += "{\\Large " + col[2].name + "}\\newline "
    else:
        outstr += "\\scalebox{%s}{"%TITLE_SCALE + col[0] + "}\\newline ";
        outstr += "\\scalebox{%s}{"%TITLE_SCALE + col[2].chords[0].copy(col[1]).latex("roman") + "}\\newline ";
        outstr += "{\\large " + col[2].name + "}\\newline "
    outstr += ", ".join([c.copy(9).latex("bullet") for c in col[2].chords])
    table[0][i+1] = outstr
# write row header
for i,row in enumerate(ROWS):
    outstr = ""
    outstr += "\\cellcolor{%s}"%COLOR_NAMES[i]
    #pitch = "\\scalebox{%s}{\\writechord{%s}}"%(TITLE_SCALE, row)
    outstr += "{\Huge %s}"%(row.replace('#','\\sharp').replace('b',"\\flat"))
    table[i+1][0] = outstr

# write contents
tikzNodes = []
for i,col in enumerate(COLUMNS):
    scale = col[2]
    for j,row in enumerate(ROWS):
        transpose = row
        outstr = scale.latex(transpose, j, "M%dM%d"%(i,j))
        table[j+1][i+1] = outstr
        parentName = ROWS_EXT[(j - scale.function)%12]
        tikzNodes.append("\\node at (%s) {%s};%%\n"%("M%dM%d"%(i,j), parentName))

# save color definitions to latex
contents = ""
contents += "\n".join(COLOR_DEFINITIONS) + "\n\n"
# save table to latex file
contents += "\\begin{tabular}{r"
for i in range(len(COLUMNS)):
    if i in COLUMN_BOLD_SEP:
        contents += 'D'
    else:
        contents += "|"
    contents += "P{4cm}"
contents += "}%\n"
for j in range(len(ROWS)+1):
    for i in range(len(COLUMNS)+1):
        if i>0:
            contents += "\n&%\n"
        contents += table[j][i] + "%"
    contents += "\n\\\\"
    if j in ROW_BOLD_SEP:
        contents += "\\thickhline"
    else:
        contents += "\\hline"
    contents += "%\n"
contents += "\\end{tabular}\n\n"

contents += "\\begin{tikzpicture}[remember picture,overlay]\n"
contents += "\n".join(tikzNodes)
contents += "\\end{tikzpicture}\n"

saveLaTeX(contents)

